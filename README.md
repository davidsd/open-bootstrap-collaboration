Open Bootstrap Collaboration
=============================

This is a wiki-based collaboration to explore the CFT bootstrap.
Everyone is welcome to take part in discussions and contribute
whatever ideas or calculations they'd like.

Click 'Wiki' in the toolbar above to view and edit the wiki.